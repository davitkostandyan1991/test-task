package org.test.task;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import org.test.task.clan.dto.ClanDTO;
import org.test.task.user.dto.UserDTO;

public class GoldAccrualTracking {
    private static final String DATE_PATERN = "yyyy-MM-dd'T'HH:mm:ss";
    private static final DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormatter.ofPattern(DATE_PATERN);
    private static final String GOLD_ACCRUAL_TRACKING_PATHNAME = "src\\main\\resources\\goldAccrualTracking.txt";
    private static final String TRACKING_ADD_MESSAGE_FORMAT =
        "User '%s' added '%d' gold to clan '%s' at time '%s'."
            + " It has been %d, and now it is %d";
    private static final String TRACKING_REDUCE_MESSAGE_FORMAT =
        "User '%s' reduced '%d' gold from clan '%s' at time '%s'."
            + " It has been %d, and now it is %d";

    public void trackAddAccrualToClan(UserDTO userDTO, ClanDTO clanDTO, int gold,
                                      LocalDateTime dateTime, int clanOldGold, int clanNewGold) {
        trackToFile(TRACKING_ADD_MESSAGE_FORMAT, userDTO, clanDTO, gold, dateTime, clanOldGold, clanNewGold);
    }

    public void trackReduceAccrualFromClan(UserDTO userDTO, ClanDTO clanDTO, int gold,
                                           LocalDateTime dateTime, int clanOldGold, int clanNewGold) {
        trackToFile(TRACKING_REDUCE_MESSAGE_FORMAT, userDTO, clanDTO, gold, dateTime, clanOldGold, clanNewGold);
    }

    private void trackToFile(String trackingMessageFormat, UserDTO userDTO, ClanDTO clanDTO, int gold,
                             LocalDateTime dateTime,
                             int clanOldGold, int clanNewGold) {
        var trackingMessage = String.format(trackingMessageFormat,
                                            userDTO.getName(),
                                            gold,
                                            clanDTO.getName(),
                                            dateTime.format(DATE_TIME_FORMATTER),
                                            clanOldGold,
                                            clanNewGold);

        var file = new File(GOLD_ACCRUAL_TRACKING_PATHNAME);
        try {
            Files.writeString(Path.of(file.toURI()),
                              trackingMessage + System.lineSeparator(),
                              StandardOpenOption.APPEND);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
