package org.test.task.user.server.impl;

import org.test.task.user.dao.impl.DefaultUserDAO;
import org.test.task.user.dto.UserDTO;
import org.test.task.user.entity.UserEntity;
import org.test.task.user.server.UserService;

public class DefaultUserService implements UserService {
    private final DefaultUserDAO userDAO;

    public DefaultUserService(DefaultUserDAO userDAO) {
        this.userDAO = userDAO;
    }

    @Override
    public UserDTO getUserById(long id) {
        var userEntity = userDAO.getUserById(id);
        return toUserDTO(userEntity);
    }

    @Override
    public void updateUserGold(UserDTO userDTO) {
        var userEntity = toUserEntity(userDTO);
        userDAO.updateUserGold(userEntity);
    }

    private UserDTO toUserDTO(UserEntity userEntity) {
        return new UserDTO(userEntity.getId(), userEntity.getName(), userEntity.getGold());
    }

    private UserEntity toUserEntity(UserDTO userDTO) {
        return new UserEntity(userDTO.getId(), userDTO.getName(), userDTO.getGold());
    }

    @Override
    public void unlock(long id) {
        userDAO.unlock(id);
    }
}
