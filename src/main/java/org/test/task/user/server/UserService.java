package org.test.task.user.server;

import org.test.task.user.dto.UserDTO;

public interface UserService {
    UserDTO getUserById(long id);

    void updateUserGold(UserDTO userEntity);

    void unlock(long id);
}
