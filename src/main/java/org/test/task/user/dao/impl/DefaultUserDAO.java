package org.test.task.user.dao.impl;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import org.test.task.user.dao.UserDAO;
import org.test.task.user.entity.UserEntity;

public class DefaultUserDAO implements UserDAO {
    private static final Map<Long, UserEntity> USERS = new ConcurrentHashMap<>();
    private static final Map<Long, Lock> USER_LOCK = new ConcurrentHashMap<>();

    static {
        initDB();
    }

    private static void initDB() {
        for (long i = 1; i <= 100; i++) {
            USERS.put(i, new UserEntity(i, "User " + i, 100));
            USER_LOCK.put(i, new ReentrantLock(true));
        }
    }

    @Override
    public UserEntity getUserById(long id) {
        USER_LOCK.get(id).lock();
        return USERS.get(id);
    }

    @Override
    public void updateUserGold(UserEntity userEntity) {
        USERS.put(userEntity.getId(), userEntity);
        USER_LOCK.get(userEntity.getId()).unlock();
    }

    @Override
    public void unlock(long id) {
        USER_LOCK.get(id).unlock();
    }
}
