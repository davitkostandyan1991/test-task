package org.test.task.user.dao;

import org.test.task.user.entity.UserEntity;

public interface UserDAO {
    UserEntity getUserById(long id);

    void updateUserGold(UserEntity userEntity);

    void unlock(long id);
}
