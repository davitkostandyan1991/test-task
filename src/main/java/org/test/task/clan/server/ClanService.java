package org.test.task.clan.server;

import org.test.task.clan.dto.ClanDTO;

public interface ClanService {
    ClanDTO getClanById(long id);

    void updateClanGold(ClanDTO clanDTO);

    void unlock(long id);
}
