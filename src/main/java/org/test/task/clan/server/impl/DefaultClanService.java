package org.test.task.clan.server.impl;

import org.test.task.clan.dao.impl.DefaultClanDAO;
import org.test.task.clan.dto.ClanDTO;
import org.test.task.clan.entity.ClanEntity;
import org.test.task.clan.server.ClanService;

public class DefaultClanService implements ClanService {
    private final DefaultClanDAO clanDAO;

    public DefaultClanService(DefaultClanDAO clanDAO) {
        this.clanDAO = clanDAO;
    }

    @Override
    public ClanDTO getClanById(long id) {
        ClanEntity clanEntity = clanDAO.getClanById(id);
        return toClanDTO(clanEntity);
    }

    @Override
    public void updateClanGold(ClanDTO clanDTO) {
        ClanEntity clanEntity = toClanEntity(clanDTO);
        clanDAO.updateClanGold(clanEntity);
    }

    @Override
    public void unlock(long id) {
        clanDAO.unlock(id);
    }

    private ClanDTO toClanDTO(ClanEntity clanEntity) {
        return new ClanDTO(clanEntity.getId(), clanEntity.getName(), clanEntity.getGold());
    }

    private ClanEntity toClanEntity(ClanDTO clanDTO) {
        return new ClanEntity(clanDTO.getId(), clanDTO.getName(), clanDTO.getGold());
    }
}
