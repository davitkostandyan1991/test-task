package org.test.task.clan.dao;

import org.test.task.clan.entity.ClanEntity;

public interface ClanDAO {
    ClanEntity getClanById(long id);

    void updateClanGold(ClanEntity clanEntity);

    void unlock(long id);
}
