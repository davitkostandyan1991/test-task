package org.test.task.clan.dao.impl;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import org.test.task.clan.dao.ClanDAO;
import org.test.task.clan.entity.ClanEntity;

public class DefaultClanDAO implements ClanDAO {
    private static final Map<Long, ClanEntity> CLANS = new ConcurrentHashMap<>();
    private static final Map<Long, Lock> CLAN_LOCK = new ConcurrentHashMap<>();

    static {
        initDB();
    }

    private static void initDB() {
        CLANS.put(1L, new ClanEntity(1, "Clan 1", 0));
        CLAN_LOCK.put(1L, new ReentrantLock(true));
        CLANS.put(2L, new ClanEntity(2, "Clan 2", 0));
        CLAN_LOCK.put(2L, new ReentrantLock(true));
    }

    @Override
    public ClanEntity getClanById(long id) {
        CLAN_LOCK.get(id).lock();
        return CLANS.get(id);
    }

    @Override
    public void updateClanGold(ClanEntity clanEntity) {
        CLANS.put(clanEntity.getId(), clanEntity);
        CLAN_LOCK.get(clanEntity.getId()).unlock();
    }

    @Override
    public void unlock(long id) {
        CLAN_LOCK.get(id).unlock();
    }
}