package org.test.task.service.impl;

import java.time.LocalDateTime;

import org.test.task.GoldAccrualTracking;
import org.test.task.clan.server.ClanService;
import org.test.task.service.AddAndReduceGoldClanService;
import org.test.task.user.server.UserService;

public class DefaultAddAndReduceGoldClanService implements AddAndReduceGoldClanService {
    private final ClanService clanService;
    private final UserService userService;
    private final GoldAccrualTracking goldAccrualTracking;

    public DefaultAddAndReduceGoldClanService(ClanService clanService, UserService userService,
                                              GoldAccrualTracking goldAccrualTracking) {
        this.clanService = clanService;
        this.userService = userService;
        this.goldAccrualTracking = goldAccrualTracking;
    }

    @Override
    public void addGoldToClan(long userId, long clanId, int gold) {
        try {
            var userDTO = userService.getUserById(userId);
            userDTO.reduceGold(gold);
            var clanDTO = clanService.getClanById(clanId);
            var clanOldGold = clanDTO.getGold();
            clanDTO.addGold(gold);
            var clanNewGold = clanDTO.getGold();
            clanService.updateClanGold(clanDTO);
            userService.updateUserGold(userDTO);
            goldAccrualTracking.trackAddAccrualToClan(userDTO, clanDTO, gold, LocalDateTime.now(),
                                                      clanOldGold, clanNewGold);
        } catch (Exception e) {
            clanService.unlock(clanId);
            userService.unlock(userId);
            e.printStackTrace();
        }
    }

    @Override
    public void reduceGoldFromClan(long userId, long clanId, int gold) {
        try {
            var user = userService.getUserById(userId);
            var clan = clanService.getClanById(clanId);
            var clanOldGold = clan.getGold();
            clan.reduceGold(gold);
            var clanNewGold = clan.getGold();
            user.addGold(gold);
            clanService.updateClanGold(clan);
            userService.updateUserGold(user);
            goldAccrualTracking.trackReduceAccrualFromClan(user, clan, gold, LocalDateTime.now(),
                                                           clanOldGold, clanNewGold);
        } catch (Exception e) {
            clanService.unlock(clanId);
            userService.unlock(userId);
            e.printStackTrace();
        }
    }
}
