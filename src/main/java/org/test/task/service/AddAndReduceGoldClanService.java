package org.test.task.service;

public interface AddAndReduceGoldClanService {
    void addGoldToClan(long userId, long clanId, int gold);

    void reduceGoldFromClan(long userId, long clanId, int gold);
}
