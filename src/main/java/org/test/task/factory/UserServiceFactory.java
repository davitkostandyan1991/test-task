package org.test.task.factory;

import org.test.task.user.dao.impl.DefaultUserDAO;
import org.test.task.user.server.UserService;
import org.test.task.user.server.impl.DefaultUserService;

public class UserServiceFactory {
    public static UserService defaultUserService() {
        return new DefaultUserService(new DefaultUserDAO());
    }
}
