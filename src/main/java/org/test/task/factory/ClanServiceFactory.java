package org.test.task.factory;

import org.test.task.clan.dao.impl.DefaultClanDAO;
import org.test.task.clan.server.ClanService;
import org.test.task.clan.server.impl.DefaultClanService;

public class ClanServiceFactory {
    public static ClanService defaultClanService() {
        return new DefaultClanService(new DefaultClanDAO());
    }
}
