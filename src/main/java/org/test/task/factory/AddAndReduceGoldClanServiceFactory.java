package org.test.task.factory;

import org.test.task.GoldAccrualTracking;
import org.test.task.clan.server.ClanService;
import org.test.task.service.AddAndReduceGoldClanService;
import org.test.task.service.impl.DefaultAddAndReduceGoldClanService;
import org.test.task.user.server.UserService;

public class AddAndReduceGoldClanServiceFactory {
    public static AddAndReduceGoldClanService defaultAddAndReduceGoldClanService(ClanService clanService,
                                                                                 UserService userService,
                                                                                 GoldAccrualTracking goldAccrualTracking) {
        return new DefaultAddAndReduceGoldClanService(clanService, userService, goldAccrualTracking);
    }
}
