package org.test.task;

import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import org.test.task.factory.AddAndReduceGoldClanServiceFactory;
import org.test.task.factory.ClanServiceFactory;
import org.test.task.factory.UserServiceFactory;

public class UserToClanAddGoldJob {
    public void execute() {
        var clanService = ClanServiceFactory.defaultClanService();
        var userService = UserServiceFactory.defaultUserService();
        var goldAccrualTracking = new GoldAccrualTracking();
        var userAddGoldToClanService =
            AddAndReduceGoldClanServiceFactory.defaultAddAndReduceGoldClanService(clanService,
                                                                                  userService,
                                                                                  goldAccrualTracking);
        var executorService = Executors.newCachedThreadPool();

        for (long i = 1; i <= 100; i++) {
            long userId = i;
            executorService.execute(() -> userAddGoldToClanService.addGoldToClan(userId, 1, 20));
        }

        for (long i = 1; i <= 100; i++) {
            long userId = i;
            executorService.execute(() -> userAddGoldToClanService.reduceGoldFromClan(userId, 1, 20));
        }

        for (long i = 1; i <= 100; i++) {
            long userId = i;
            executorService.execute(() -> userAddGoldToClanService.addGoldToClan(userId, 2, 20));
        }

        for (long i = 1; i <= 100; i++) {
            long userId = i;
            executorService.execute(() -> userAddGoldToClanService.reduceGoldFromClan(userId, 2, 20));
        }

        executorService.shutdown();

        try {
            executorService.awaitTermination(10, TimeUnit.SECONDS);
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
            e.printStackTrace();
        }

        var clanGold1 = clanService.getClanById(1).getGold();
        var clanGold2 = clanService.getClanById(2).getGold();
        int userGold1 = userService.getUserById(1).getGold();
        int userGold2 = userService.getUserById(2).getGold();
        if (clanGold1 == 0 && clanGold2 == 0 && userGold1 == 100 && userGold2 == 100) {
            System.out.println("The clan is filled correctly!");
        } else {
            System.out.println("The clan is filled not correctly!");
        }
    }
}
